# Sammel-App

Der Nightly Build des Prototypen, der ohne Server läuft liegt [hier](https://gitlab.com/kybernetik/sammel-app/-/jobs/artifacts/master/raw/downloads/sammel_app/sammel-app-nightly.apk?job=build_android_apk).
(offline wenn gerade eine CI-Pipeline läuft)

# Projekt aufsetzen

Eine umfassende Anleitung zum Aufsetzen des Projekts findet sich [hier](https://gitlab.com/kybernetik/sammel-app/wikis/Projekt-aufsetzen).

# Test-Coverage

Die aktuelle Test-Coverage für die App liegt [hier](https://kybernetik.gitlab.io/sammel-app/coverage/app).

Die aktuelle Test-Coverage für den Server liegt [hier](https://kybernetik.gitlab.io/sammel-app/coverage/server).

# Lizenz

Der Code und alle Inhalte dieses Projekts, inklusive Bilder und Dokumentationen, sowie des Codes und der Docker-Images sind Open-Source und können frei und unentgeltlich genutzt und weiterentwickelt werden. Die  weiterentwickelten und abgeleiteten Werke müssen dabei jedoch unter den selben Bedingungen lizenisert werden und der Quellcode öffentlich zugänglich gemacht werden.

Die Inhalte stehen unter der [GNU Public License](https://www.gnu.org/licenses/gpl-3.0.de.html).

Folgende Ausnahmen bestehen für die freie Nutzung der Inhalte, des Codes und der App: Der Code und alle weiteren Inhalte dieses Projekts können nicht genutzt werden für politische Initiativen die sexistische, rassistische, antisemitische, nationalistische, anti-soziale, umweltfeindliche oder militärische Ziele verfolgen. Um Sicherheit über die freie Verwendung der Inhalte dieses Projekts zu haben, empfiehlt es sich mit den Entwickler*innen in Kontakt zu treten unter [dwesammelapp@googlemail.com](mail:dwesammelapp@googlemail.com).
